FROM openjdk:11
EXPOSE 8083
COPY target/5ARCTIC5-G5-GTI.jar ci_target/myspringapp.jar
ENTRYPOINT ["java", "-jar", "/myspringapp.jar"]
